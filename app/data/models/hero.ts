import {Person} from "./person.ts";
export class Hero {
    constructor(person: Person, comment: string){
        this.promotionDate = new Date();
        this.person = person;
        this.comment = comment;
    }

    public id: number;
    public person: Person;
    public comment: string;
    public promotionDate: Date;
}