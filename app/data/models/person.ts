export class Person {
    constructor(id: string, name: string, gender: Gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    public id: string;
    public name: string;
    public gender: Gender;
    public picture: string;

    public getPicture() {
        let imageFile = this.picture && this.picture.length > 0
            ? this.picture
            : this.gender === Gender.Man ? "man.png" : "woman.png";

        return "img/" + imageFile;
    }
}

export enum Gender {
    Man,
    Woman
}

