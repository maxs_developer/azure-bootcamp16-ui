import {Injectable} from "angular2/core";
import {Person, Gender} from "../data/models/person.ts";
import {Hero} from "../data/models/hero.ts";

export interface IPersonService {
    getAll(): Promise<Array<Person>>;
}

@Injectable()
export class PersonService implements IPersonService {
    people: Array<Person>;

    constructor() {
        this.people = [
            new Person("1", "John Doe", Gender.Man),
            new Person("2", "Jane Doe", Gender.Woman),
            new Person("3", "Janie Doe", Gender.Woman),
            new Person("4", "John Roe", Gender.Man),
            new Person("5", "Johnnie Doe", Gender.Man),
        ];
    }

    public getAll() {
        return new Promise<Array<Person>>(resolve =>
            setTimeout(() => resolve(this.people), 500)
        );
    }
}
