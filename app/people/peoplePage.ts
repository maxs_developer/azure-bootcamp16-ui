import {Page, Modal, NavController, NavParams} from "ionic-angular";
import {Person, Gender} from "../data/models/person.ts";
import {PersonService} from "./person.service.ts";
import {HeroService} from "../heroes/heroes.service.ts";
import {PromotionCommentModal} from "../pages/promotionCommentModal.ts";
import {LatestHeroesPage} from "../heroes/latestHeroesPage.ts";

@Page({
    templateUrl: "build/people/peoplePage.html"
})
export class PeoplePage {
    public people: Array<Person>;

    constructor(private _nav: NavController,
        private _personService: PersonService,
        private _heroService: HeroService) {
        this.loadPeople(null);
    }

    loadPeople(refresher) {
        this._personService.getAll().then(people => {
            this.people = people;

            if (refresher) {
                refresher.complete();
            }
        });
    }

    promote(person: Person) {
        let profileModal = Modal.create(PromotionCommentModal, { person: person });
        profileModal.onDismiss(data => {
            if (data) {
                this._heroService.promote(data.person, data.comment);
                console.log(person.name + " was promoted with comment: " + data.comment);
            }
        });

        this._nav.present(profileModal);

    }
}


