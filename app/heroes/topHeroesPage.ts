import {Page} from "ionic-angular";
import {Hero} from "../data/models/hero.ts";
import {HeroService} from "./heroes.service.ts";

@Page({
    templateUrl: "build/heroes/topHeroesPage.html"
})
export class TopHeroesPage {
    public heroes: Array<Hero>;

    constructor(private _heroService: HeroService) {
        this.loadHeroes(null);
    }

    loadHeroes(refresher) {
        this._heroService.getTop().then(heroes => {
            this.heroes = heroes;

            if (refresher) {
                refresher.complete();
            }
        });
    }
}
