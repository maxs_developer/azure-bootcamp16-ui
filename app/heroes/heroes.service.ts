import {Injectable} from "angular2/core";
import {Person, Gender} from "../data/models/person.ts";
import {Hero} from "../data/models/hero.ts";

export interface IHeroService {
    promote(person: Person, comment: string);
    getTop(count: number): Promise<Array<Hero>>;
    getLatest(count: number): Promise<Array<Hero>>;
}

@Injectable()
export class HeroService implements IHeroService {
    heroes: Array<Hero>;
    constructor() {
        this.heroes = [];
    }

    public promote(person: Person, comment: string) {
        //TODO: validate comment and person

        let hero = new Hero(person, comment);
        this.heroes.push(hero);

        console.log("Current heroes", this.heroes);
    }

    public getTop(count: number = 10) {
        //TODO: filter

        return new Promise<Array<Hero>>(resolve =>
            setTimeout(() => resolve(this.heroes), 2000) // 2 seconds
        );
    }

    public getLatest(count: number = 10) {
        //TODO: filter

        return new Promise<Array<Hero>>(resolve =>
            setTimeout(() => resolve(this.heroes), 2000) // 2 seconds
        );
    }
}