import {Modal, ViewController, NavController, NavParams} from "ionic-angular";
import {Page} from "ionic-angular";
import {Person} from "../data/models/person.ts";

@Page({
    templateUrl: "build/pages/promotionCommentModal.html"
})

export class PromotionCommentModal {
    person: Person;
    comment: string;

    constructor(private _viewCtrl: ViewController,
                private _params: NavParams) {

        this.person = this._params.get("person");
    }

    post() {
        if (this.comment) {
            this._viewCtrl.dismiss({ person: this.person, comment: this.comment });
        }
    }

    cancel() {
        this._viewCtrl.dismiss();
    }
}