import {Page} from "ionic-angular";
import {TopHeroesPage} from "../../heroes/topHeroesPage";
import {LatestHeroesPage} from "../../heroes/latestHeroesPage";
import {PeoplePage} from "../../people/peoplePage";


@Page({
  templateUrl: "build/pages/tabs/tabs.html"
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = TopHeroesPage;
  tab2Root: any = LatestHeroesPage;
  tab3Root: any = PeoplePage;
}
